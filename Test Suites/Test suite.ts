<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8fdeee56-376c-405e-a36d-f7e3843cdae4</testSuiteGuid>
   <testCaseLink>
      <guid>163ac5b0-1cd1-4642-be4f-abb73f252e79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Adding product mechanism test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14074953-9ef0-428b-8a3d-90fac0f02f8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin role see transactions test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00be76f7-d708-48e7-bdc7-36bcc36bf15d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Changing product amount total price test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>472a22ac-5d70-49b5-90bd-174e5c1e580b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Check available product for User role test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3f67946-e2f6-4dba-9531-c46c25782959</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Log in then log out test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e29722d-8a91-4641-975c-d92166e6dd3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login by Admin role test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>277f609c-e46a-4ef1-b537-ba93f27e9c70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login by incorrect username and password test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1116c87-7978-4633-8973-86cd1539e5ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login by User role test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c9bda0a-44dd-421f-8676-6a67ce8e7fd9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login page test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d7d3919-c465-4c68-b7ee-4c59916182a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Select then deselect of password input box test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6760ccd3-964c-48ae-a992-61980dab6fa2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Select then deselect of username input box test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>baf3f191-6931-42c2-8809-b8daf27c3f0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/User role adds transaction test case</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
