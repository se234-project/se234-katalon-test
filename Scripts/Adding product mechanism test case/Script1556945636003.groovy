import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.85.148.120:8081/')

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_1'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_2'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_3'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_4'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_5'))

WebUI.click(findTestObject('Page_ProjectBackend/a_Carts'))

WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="add-row"]/div/table'))

'To locate group of card'
List<WebElement> rows = table.findElements(By.tagName('tr'))

println('No. of products: ' + rows.size())

'Compare the value'
WebUI.verifyEqual(6, rows.size())

WebUI.closeBrowser()

