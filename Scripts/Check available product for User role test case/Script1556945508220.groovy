import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.85.148.120:8081/')

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_Login'))

WebUI.waitForElementVisible(findTestObject('Page_ProjectBackend/AvailableProducts'), 0)

WebDriver driver = DriverFactory.getWebDriver()

WebElement cardsGroup = driver.findElement(By.xpath('/html/body/app-root/app-product-list/div/div[2]/div'))

'To locate group of card'
List<WebElement> cards = cardsGroup.findElements(By.className('farmer-card'))

println('No. of products: ' + cards.size())

'Compare the value'
WebUI.verifyEqual(5, cards.size())

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/h5_Garden'), 'Garden')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/h5_Banana'), 'Banana')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/h5_Orange'), 'Orange')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/h5_Papaya'), 'Papaya')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/h5_Rambutan'), 'Rambutan')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_Logout'))

WebUI.closeBrowser()

